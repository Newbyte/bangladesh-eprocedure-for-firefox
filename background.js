"use strict";

const SKYPE_URLS = [ "https://www.eprocure.gov.bd/*" ];
const FIREFOX_USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0";

function rewriteUserAgentHeader(evnt) {
    for (let i = 0; i < evnt.requestHeaders.length; i++) {
        if (evnt.requestHeaders[i].name.toLowerCase() === "user-agent") {
            evnt.requestHeaders[i].value = FIREFOX_USER_AGENT;
        }
    }

    return { requestHeaders: evnt.requestHeaders };
}

browser.webRequest.onBeforeSendHeaders.addListener(
    rewriteUserAgentHeader,
    { urls: SKYPE_URLS },
    [ "blocking", "requestHeaders" ]
);
